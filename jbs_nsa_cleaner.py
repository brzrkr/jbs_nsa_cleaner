"""
MIT License

Copyright (c) 2020 Federico Salerno

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os
import untangle
import subprocess
import psutil


settings_dir = os.path.join(os.getenv("APPDATA"), "johnsadventures.com", "Background Switcher")
nsa_file_path = os.path.join(settings_dir, "NeverShowAgain.xml")

settings = untangle.parse(nsa_file_path)
pictures = settings.JBSData.never_show_again.picture

if __name__ == '__main__':
    # Delete pics.
    for picture in pictures:
        pic_path = picture.cdata
        try:
            os.remove(pic_path)
        except OSError:
            print(f"Error when deleting {pic_path}")
            continue
    
    # Delete NeverShowAgain.xml to clear list.
    try:
        os.remove(nsa_file_path)
    except OSError:
        print("Could not remove NeverShowAgain.xml: clear the list manually, "
              "then restart BackgroundSwitcher.exe.")
        exit(1)
    
    # Restart BackgroundSwitcher.exe.
    switcher = None
    rerun_command = ""
    for process in psutil.process_iter(["name"]):
        if process.info["name"] == "BackgroundSwitcher.exe":
            switcher = process
            rerun_command = process.cmdline()
            break
    else:
        # BackgroundSwitcher.exe is not running. Assume this is normal.
        exit(0)
    
    try:
        switcher.terminate()
    except OSError:
        print("Could not terminate BackgroundSwitcher.exe: restart it manually.")
        exit(1)
    
    try:
        subprocess.Popen(rerun_command)
    except OSError:
        print("BackgroundSwitcher.exe was stopped but could not be restarted:"
              " start it manually.")
        exit(2)
