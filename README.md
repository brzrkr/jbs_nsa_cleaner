# Usage
Just run jbs_nsa_cleaner with Python 3:
`python -m jbs_nsa_cleaner.py`

Ensure that your Python environment has the necessary requirements listed in
 `requirements.txt`.  
All requirements are installable with `pip install`.
